import 'dart:io';
import 'dart:math';

void main(List<String> arguments) {
  Game game = Game();
  print("============ ᴍɪɴᴇsᴡᴇᴇᴘᴇʀ ============​");
  game.showItemDetail();
  game.newBoard();
  while (true) {
    game.showTable();
    game.inputRowCol();
    print('========================');
    if (game.isDeath()) {
      game.showTable();
      print(
          '\u{1F4A5}\u{1F4A5}\u{1F4A3} Boooooom!!!! \u{1F4A3}\u{1F4A5}\u{1F4A5}');
      if (game.playAgain()) {
        game.newBoard();
      } else {
        return;
      }
    }
    if (game.isFinish()) {
      game.showTable();
      print('\u{2728}\u{1F38A} Victory!!!!! \u{1F38A}\u{2728}');
      if (game.playAgain()) {
        game.newBoard();
      } else {
        return;
      }
    }
  }
}

abstract class Box {
  String symbol = "\u{25FC}";
  int row = 0;
  int column = 0;

  String get getSymbol {
    return symbol;
  }

  int get getRow {
    return row;
  }

  int get getColumn {
    return column;
  }

  set setSymbol(String symbol) {
    this.symbol = symbol;
  }
}

class Bomb extends Box {
  @override
  int row;
  @override
  int column;

  Bomb(this.row, this.column);
}

class Empty extends Box {
  int bombsAround = 0;
  @override
  int row;
  @override
  int column;

  Empty(this.row, this.column);

  int get getBombs {
    return bombsAround;
  }

  set setBombsAround(int numberOfBombs) {
    bombsAround = numberOfBombs;
  }
}

class Item extends Box {
  String itemType;
  bool useItem = false;
  @override
  int row;
  @override
  int column;

  String get getItemType {
    return itemType;
  }

  bool get getUse {
    return useItem;
  }

  set setUse(bool useItem) {
    this.useItem = useItem;
  }

  Item(this.row, this.column, this.itemType);
}

class Board {
  List<Box> table = [];
  int row;
  int column;

  List<Box> get getTable {
    return table;
  }

  int get getRow {
    return row;
  }

  int get getColumn {
    return column;
  }

  Board(this.row, this.column) {
    for (int i = 0; i < row; i++) {
      for (int j = 0; j < column; j++) {
        table.add(Empty(i, j));
      }
    }
  }

  void openBoxAround(int row, int col) {
    if (row > 0) {
      if (table[getIndex1D(row - 1, col)].getSymbol != ' ') {
        openBox(row - 1, col);
      }
    }
    if (col > 0) {
      if (table[getIndex1D(row, col - 1)].getSymbol != ' ') {
        openBox(row, col - 1);
      }
    }
    table[getIndex1D(row, col)].setSymbol = " ";

    if (col < getColumn - 1) {
      if (table[getIndex1D(row, col + 1)].getSymbol != ' ') {
        openBox(row, col + 1);
      }
    }

    if (row < getRow - 1) {
      if (table[getIndex1D(row + 1, col)].getSymbol != ' ') {
        openBox(row + 1, col);
      }
    }
    if (row > 0 && col > 0) {
      if (table[getIndex1D(row - 1, col - 1)].getSymbol != ' ') {
        openBox(row - 1, col - 1);
      }
    }
    if (row < getRow - 1 && col < getColumn - 1) {
      if (table[getIndex1D(row + 1, col + 1)].getSymbol != ' ') {
        openBox(row + 1, col + 1);
      }
    }
    if (row < getRow - 1 && col > 0) {
      if (table[getIndex1D(row + 1, col - 1)].getSymbol != ' ') {
        openBox(row + 1, col - 1);
      }
    }
    if (row > 0 && col < getColumn - 1) {
      if (table[getIndex1D(row - 1, col + 1)].getSymbol != ' ') {
        openBox(row - 1, col + 1);
      }
    }
  }

  void openBox(int row, int col) {
    //if Item
    if (table[getIndex1D(row, col)] is Item) {
      if ((table[getIndex1D(row, col)] as Item).getUse != true) {
        openBoxAround(row, col);
        String symbol = (table[getIndex1D(row, col)] as Item).getItemType;
        table[getIndex1D(row, col)].setSymbol = symbol;
      }
    }

    //if Empty
    if (table[getIndex1D(row, col)] is Empty) {
      Empty empty = table[getIndex1D(row, col)] as Empty;
      if (empty.getBombs != 0) {
        int num = empty.getBombs;
        table[getIndex1D(row, col)].setSymbol = "$num";
      }
      if (empty.getBombs == 0) {
        openBoxAround(row, col);
        table[getIndex1D(row, col)].setSymbol = " ";
      }
    }
  }

  void setBombs(int bombs) {
    int bombProbability = 1;
    int maxProbability = 10;
    Random random = Random();

    for (int i = 0; i < row; i++) {
      for (int j = 0; j < column; j++) {
        int randomNumber = random.nextInt(maxProbability);
        if (randomNumber < bombProbability &&
            table[getIndex1D(i, j)] is Empty) {
          table[getIndex1D(i, j)] = Bomb(i, j);
          bombs++;
        }
        if (bombs == 10) {
          return;
        }
      }
    }
    if (bombs != 10) {
      setBombs(bombs);
    }
  }

  void setItems(int items) {
    int itemProbability = 2;
    int maxItemProbability = 15;
    int maxTypeProbability = 3;
    Random random = Random();

    for (int i = 0; i < row; i++) {
      for (int j = 0; j < column; j++) {
        if (table[getIndex1D(i, j)] is Empty &&
            (table[getIndex1D(i, j)] as Empty).getBombs == 0) {
          int randomNumberItem = random.nextInt(maxItemProbability);
          if (randomNumberItem < itemProbability) {
            int randomNumberType = random.nextInt(maxTypeProbability);
            if (randomNumberType < itemProbability) {
              table[getIndex1D(i, j)] = Item(i, j, "?");
              items++;
            } else {
              table[getIndex1D(i, j)] = Item(i, j, "♥️");
              items++;
            }
          }
        }
        if (items == 2) {
          return;
        }
      }
    }
    if (items != 2) {
      setItems(items);
    }
  }

  void addBombNumber(int row, int col) {
    int num = (table[getIndex1D(row, col)] as Empty).getBombs + 1;
    (table[getIndex1D(row, col)] as Empty).setBombsAround = num;
  }

  void countBombAround() {
    for (int i = 0; i < row; i++) {
      for (int j = 0; j < column; j++) {
        if (table[getIndex1D(i, j)] is Empty) {
          if (i > 0 && j > 0) {
            if (table[getIndex1D(i - 1, j - 1)] is Bomb) {
              addBombNumber(i, j);
            }
          }

          if (i > 0) {
            if (table[getIndex1D(i - 1, j)] is Bomb) {
              addBombNumber(i, j);
            }
          }

          if (i > 0 && j < column - 1) {
            if (table[getIndex1D(i - 1, j + 1)] is Bomb) {
              addBombNumber(i, j);
            }
          }

          if (j > 0) {
            if (table[getIndex1D(i, j - 1)] is Bomb) {
              addBombNumber(i, j);
            }
          }

          if (j < column - 1) {
            if (table[getIndex1D(i, j + 1)] is Bomb) {
              addBombNumber(i, j);
            }
          }

          if (i < row - 1 && j > 0) {
            if (table[getIndex1D(i + 1, j - 1)] is Bomb) {
              addBombNumber(i, j);
            }
          }

          if (i < row - 1) {
            if (table[getIndex1D(i + 1, j)] is Bomb) {
              addBombNumber(i, j);
            }
          }

          if (i < row - 1 && j < column - 1) {
            if (table[getIndex1D(i + 1, j + 1)] is Bomb) {
              addBombNumber(i, j);
            }
          }
        }
      }
    }
  }

  void useItem() {
    for (int i = 0; i < row; i++) {
      for (int j = 0; j < column; j++) {
        if (table[getIndex1D(i, j)] is Bomb &&
            table[getIndex1D(i, j)].getSymbol == "\u{25FC}") {
          table[getIndex1D(i, j)].setSymbol = "⚐";
          print(
              '\u{2753}\u{1F6A9} Mark Flag Bombs at row:$i col:$j \u{1F6A9}\u{2753}');
          return;
        }
      }
    }
  }

  void showBombs() {
    for (int i = 0; i < row; i++) {
      for (int j = 0; j < column; j++) {
        if (table[getIndex1D(i, j)] is Bomb) {
          table[getIndex1D(i, j)].setSymbol = "☢";
        }
      }
    }
  }
}

class Game {
  Board board = Board(8, 10);
  bool death = false;
  int life = 3;
  int flag = 10;
  num startTime = 0;
  num endTime = 0;
  num bestTime = 2147483647; //Max Integer

  int get getLife {
    return life;
  }

  set setLife(int life) {
    this.life = life;
  }

  void newBoard() {
    board = Board(8, 10);
    board.setBombs(0);
    board.countBombAround();
    board.setItems(0);
    death = false;
    life = 3;
    flag = 10;
    startTime = DateTime.now().millisecondsSinceEpoch / 1000;
  }

  void showTable() {
    List<Box> table = board.getTable;
    if (bestTime != 2147483647) {
      print(
          'Best Time: ${bestTime.toStringAsFixed(2)} second \u{1F6A9} x $flag ❤️  x $life');
    } else {
      print('Best Time: -  \u{1F6A9} x $flag ❤️  x $life');
    }
    stdout.write('  ┃');
    for (int j = 0; j < board.getColumn; j++) {
      stdout.write('$j ');
    }
    print('');
    stdout.write('━━┃');
    for (int j = 0; j < board.getColumn; j++) {
      stdout.write('━━');
    }
    print('');
    for (int i = 0; i < board.getRow; i++) {
      i < 10 ? stdout.write(' $i┃') : stdout.write('$i|');
      for (int j = 0; j < board.getColumn; j++) {
        stdout.write('${table[getIndex1D(i, j)].getSymbol} ');
      }
      print('');
    }
    print('');
  }

  void inputRowCol() {
    List<Box> table = board.getTable;
    print('Please input row:');
    var r = stdin.readLineSync()!;
    if (!checkInput(r)) {
      return;
    }
    print('Please input column:');
    var c = stdin.readLineSync()!;
    if (!checkInput(c)) {
      return;
    }
    print('========================');

    int row = int.parse(r);
    int col = int.parse(c);
    if (!checkRange(row, col)) {
      return;
    }
    if ((table[getIndex1D(row, col)] is Item) &&
        table[getIndex1D(row, col)].getSymbol != "\u{25FC}" &&
        table[getIndex1D(row, col)].getSymbol != " ") {
      needUseItem(row, col);
      return;
    }

    openOrFlag(row, col);
  }

  bool checkInput(var num) {
    final RegExp numRegExp = RegExp(r'\d');
    if (numRegExp.hasMatch(num)) {
      return true;
    }
    print('Invalid Input!! \u{274C}');
    return false;
  }

  bool checkRange(int row, int col) {
    if (row < 0 || row > board.getRow || col < 0 || col > board.getColumn) {
      print('Input Row Column Incorrect\u{2757}\u{2757}');
      print('Range of Row: 0-${board.getRow - 1}');
      print('Range of Column: 0-${board.getColumn - 1}');
      return false;
    }
    return true;
  }

  void openOrFlag(int row, int col) {
    List<Box> table = board.getTable;
    if (table[getIndex1D(row, col)].getSymbol == '\u{25FC}') {
      print('Open Box \u{25FC} or Bomb Flag \u{1F6A9} ? (o/f)');
      String str = stdin.readLineSync()!;
      if (str == 'f') {
        if (flag > 0) {
          table[getIndex1D(row, col)].setSymbol = "⚐";
          print(
              '\u{1F6A9}\u{1F6A9} You mark Flag Bombs at row:$row col:$col \u{1F6A9}\u{1F6A9}');
          flag--;
          return;
        } else {
          print('\u{1F6A9} You mark Flag Bombs max 10 flag!!');
        }
      } else if (str == 'o') {
        if (table[getIndex1D(row, col)] is Bomb) {
          table[getIndex1D(row, col)].setSymbol = "☢";
          print('Oops!! Hit a bomb \u{1F4A5}\u{1F4A5} -1 ❤️');
          life == 0 ? death = true : life--;
          flag--;
          return;
        }
        board.openBox(row, col);
      } else {
        print('Invalid Input!! \u{274C}');
      }
    } else if (table[getIndex1D(row, col)].getSymbol == '⚐') {
      print("\u{1F6A9} Do you want unmark Flag Bomb??(y/n)");
      String str = stdin.readLineSync()!;
      if (str == 'y') {
        table[getIndex1D(row, col)].setSymbol = '\u{25FC}';
        flag++;
      } else if (str != 'n') {
        print('Invalid Input!! \u{274C}');
      }
    } else {
      print('\u{2757}\u{2757}This box opened \u{2757}\u{2757}');
    }
  }

  bool isDeath() {
    if (death) {
      board.showBombs();
      return true;
    }
    return false;
  }

  bool playAgain() {
    print('Try again? (y/n)');
    String str = stdin.readLineSync()!;
    if (str == 'y') {
      return true;
    } else if (str == 'n') {
      return false;
    }
    return false;
  }

  bool isFinish() {
    List<Box> table = board.getTable;
    for (int i = 0; i < board.getRow; i++) {
      for (int j = 0; j < board.getColumn; j++) {
        if (table[getIndex1D(i, j)].getSymbol == '\u{25FC}' &&
            (table[getIndex1D(i, j)] is Empty ||
                table[getIndex1D(i, j)] is Item)) {
          return false;
        }
      }
    }
    isBestTime();
    return true;
  }

  void needUseItem(int row, int col) {
    List<Box> table = board.getTable;
    if ((table[getIndex1D(row, col)] as Item).getItemType == '?') {
      print("Use This Item?(y/n)");
      String str = stdin.readLineSync()!;
      if (str == 'y') {
        table[getIndex1D(row, col)].setSymbol = " ";
        board.useItem();
        (table[getIndex1D(row, col)] as Item).setUse = true;
        flag--;
      } else if (str == 'n') {
        return;
      } else {
        print('Invalid Input!! \u{274C}');
      }
    } else if ((table[getIndex1D(row, col)] as Item).getItemType == '♥️') {
      table[getIndex1D(row, col)].setSymbol = " ";
      if (life < 3) {
        life += 1;
        print('Yeah!! \u{1F44D}\u{1F44D}  +1  ❤️');
      }
    }
  }

  void showItemDetail() {
    print("Item Detail: ? → \u{1F6A9} Mark Flag Bomb on table 1 flag.");
    print("             ♥️ → ❤️  Increase 1 Life(Max Life: 3).");
    print('========================');
  }

  void isBestTime() {
    endTime = DateTime.now().millisecondsSinceEpoch / 1000;
    num totalTime = endTime - startTime;
    if (totalTime < bestTime) {
      bestTime = totalTime;
    }
  }
}

int getIndex1D(int row, int col) {
  return row * 10 + col;
}
